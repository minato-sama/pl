import java.util.*
fun Numq(num: String): Boolean {
    for (ch in num) {
        if (ch.isDigit()) {
            return true
        }
    }
    return false
}
fun main() {
    val ops = arrayOf("+", "-", "*", "/")
    val parts = readLine()?.trim()?.split(" ")
    val stack = Stack<String>()
    if (parts != null) {
        for (part in parts.reversed()) {
            if (Numq(part)) {
                stack.push(part);
            } else {
                if (part in ops) {
                    if (stack.size >= 2) {
                        val op1 = stack.pop()
                        val op2 = stack.pop()
                        val res = "($op1 $part $op2)"
                        stack.push(res)
                    } else {
                        print("не хватает символов: ")
                        return
                    }
                } else {
                    println("неверная операция")
                    return
                }
            }
        }
    }

    if (stack.empty()) {
        println("пустой стэк")
    } else if (stack.size > 1) {
        println("в стэке более 1 элемента")
    } else {
        println(stack.pop())
    }
}
